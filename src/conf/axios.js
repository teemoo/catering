import _ from 'lodash';
import { Indicator } from 'mint-ui';

export default function (axios) {
	// Add a request interceptor
	axios.interceptors.request.use(config => {
	    // Do something before request is sent
	    var defaultOptions={
	    	hotelGroupCode:'GCBZG',
	    	hotelCode:'GCBZ',
	    	pccode:'110'
	    }
	    if(config.method=='get'){
	    	
	    	config.params=_.merge({},defaultOptions,config.params)

	    }else if(config.method=='post'){
	    	config.data=_.merge({},config.data)
	    }
	    Indicator.open();
	    return config;
	  }, error => {
	    // Do something with request error
	    return Promise.reject(error);
	  });
	// Add a response interceptor
	axios.interceptors.response.use(function (response) {
	    // Do something with response data
	    Indicator.close();
	    return response;
	  }, function (error) {
	    // Do something with response error
	    return Promise.reject(error);
	  });
	}