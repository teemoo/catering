// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import MintUI from 'mint-ui'
import axios from 'axios'
import axconf from './conf/axios'
import '../static/js/lib-flexible/build/flexible.js'
import '../static/css/reset.css'
import './assets/css/mint-ui-style.css'
import '../static/fonts/iconfont.css'
import store from './store'
import vueTap from 'v-tap';
import VueSocketio from 'vue-socket.io';

Vue.use(VueSocketio, 'http://localhost');
Vue.use(vueTap);
Vue.use(MintUI);
Vue.config.productionTip = false

axconf(axios)
Vue.prototype.$http = axios

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  sockets:{
	connect: function(){
	  this.id=this.$socket.id
	},
	customEmit: function(val){
	  console.log('this method was fired by the socket server. eg: io.emit("customEmit", data)')
	}
   },
})
