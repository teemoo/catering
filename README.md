# 绿云餐饮前端

> A FE project for greenCloud catering

## 安装

node (略)

## 构建步骤 Build Setup 

``` bash
# install dependencies
npm install

# 开发使用 serve with hot reload at localhost:8080
npm run dev

# 生产环境使用 build for production with minification
npm run build （dist目录放在容器下即可)

# build for production and view the bundle analyzer report
npm run build --report
```



