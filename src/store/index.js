import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        count: 1,
        pageDirection: 'fade',
        routeChain:[],
        peopleNumber:1,
        carList:[]
    },
    mutations: {
        addRouteChain(state, route){
            state.routeChain.push(route);
        },
        popRouteChain(state){
            state.routeChain.pop();
        },
        setPageDirection(state, dir){
            state.pageDirection = dir;
        },
        setNumber(state, number){
            state.peopleNumber = number;
        },
        addCarList(state,food){
            
            var index = _.findIndex(state.carList, food);
 
            if(index>-1){
              
                if (food.count>0) {
                    _.fill(state.carList,food,index,index)
                }else{
                    state.carList.splice(index,1)
                }
            }else{
                state.carList.push(food);
            }

        }
    }
});

export default store;